require 'spec_helper'

describe List do
  it "has valid factory" do
    FactoryGirl.create(:list).should be_valid
  end

  it "is invalid without uuid" do
    FactoryGirl.build(:list, :without_uuid).should_not be_valid
  end

  it "returns uuid as title if title is nil" do
    list = FactoryGirl.build(:list)
    list.get_title.should == list.uuid
  end

  it "returns uuid as title if title is empty" do
    list = FactoryGirl.build(:list, :title => '')
    list.get_title.should == list.uuid
  end

  it "returns title if title is present" do
    list = FactoryGirl.build(:list, :title => "List")
    list.get_title.should == list.title
  end
end
