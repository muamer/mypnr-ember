require 'spec_helper'

describe Task do
  it "has valid factory" do
    FactoryGirl.build(:task).should be_valid
  end

  it "is invalid without content" do
    FactoryGirl.build(:task, :content => nil).should_not be_valid
  end

  it "is invalid with empty content" do
    FactoryGirl.build(:task, :content => "").should_not be_valid
  end

  it "is invalid with empty list_id" do
    FactoryGirl.build(:task, :list_id => nil).should_not be_valid
  end

  it "is invalid without quadrant" do
    FactoryGirl.build(:task, :quadrant => nil).should_not be_valid
  end

  it "is invalid without completed" do
    FactoryGirl.build(:task, :completed => nil).should_not be_valid
  end

  it "returns content as string" do
    task = FactoryGirl.create(:task)
    task.content.should == task.content
  end
end
