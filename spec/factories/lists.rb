# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :list do
    uuid SecureRandom.uuid

    trait :without_uuid do
      uuid nil
    end

    trait :with_tasks do
      after :create do |list|
        FactoryGirl.create_list :task, 5, :list => list
      end
    end
  end
end
