# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  sequence(:content) do |n|
    "Task #{n}"
  end

  factory :task do
    content
    quadrant 1
    completed false
    list
  end
end
