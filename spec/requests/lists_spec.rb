require 'spec_helper'

describe "Lists" do
  describe "GET /lists" do
    it "displays lists" do
      list = FactoryGirl.create(:list)
      visit lists_path
      page.should have_content list.uuid
    end
  end

  describe "GET /list/1", :focus do
    before(:all) do
      @list = FactoryGirl.create(:list, :with_tasks)
      visit list_path(@list)
    end

    it "displays list tasks" do
      page.should have_content('Task')
    end

    it "should have hidden list edit form" do
      page.all(".edit_list").should_not be_visibe
    end
  end

  describe "GET /lists/new" do
    it "automatically creates new lists" do
      expect {
        visit new_list_path
      }.to change(List, :count).by(1)
    end

    it "shows form on list title click" do
      find(:css, "h1.listTitle").click
      page.find(:css, ".listForm").should be_visibe
    end
  end
end
