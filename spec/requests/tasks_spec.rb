require 'spec_helper'

describe "Tasks" do
  before :all do
    @list = FactoryGirl.create(:list)
  end

  describe "GET /new" do
    it "renders form in quadrant" do
      visit list_path(@list)
      within "#2" do
        page.should have_css("#new_task")
      end
    end

    it "renders form in quadrant for provided id" do
      visit list_path(@list, quadrant: 1)
      within "#1" do
        page.should have_css("#new_task")
      end
    end
  end

  describe "POST /tasks" do
    it "saves new task" do
      visit list_path(@list)
      within "#new_task" do
        fill_in "task_content", with: "Task content"
        click_button "Save Task"
      end
      page.should have_content("Task content")
    end

    it "shows form in saved task's quadrant" do
      visit list_path(@list)
      within "#new_task" do
        fill_in "task_content", with: "Task content"
        find(:xpath, "//input[@id='task_quadrant']").set "1"
        click_button "Save Task"
      end
      page.should have_content("Task content")
      page.should have_css("#1 #new_task")
    end
  end

  describe "DELETE /task/1" do
    it "should delete task" do
      @list = FactoryGirl.create(:list, :with_tasks)
      task = @list.tasks.last

      visit list_path(@list)
      find(:css, "#task-#{task.id} .deleteTask").click
      page.should have_no_content task.content
    end
  end
end
