require 'spec_helper'

describe TasksController do
  describe 'GET /complete/1' do
    it "completes task" do
      task = FactoryGirl.create(:task)
      expect {
        get :complete, :id => task, :list_id => task.list
        task.reload
      }.to change { task.completed }.from(false).to(true)
    end

    it "toggles completed from true to false" do
      task = FactoryGirl.create(:task)
      expect {
        get :complete, :id => task, :list_id => task.list
        task.reload
      }.to change { task.completed }.from(false).to(true)
    end

    it "toggles completed from false to true" do
      task = FactoryGirl.create(:task, :completed => true)
      expect {
        get :complete, :id => task, :list_id => task.list
        task.reload
      }.to change { task.completed }.from(true).to(false)
    end
  end
end
