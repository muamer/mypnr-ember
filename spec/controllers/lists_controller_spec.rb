require 'spec_helper'

describe ListsController do
  describe "GET #index" do
  end

  describe "GET /show/1" do
    it "renders :show view" do
      list = FactoryGirl.create(:list)
      get :show, :id => list
      response.should render_template :show
    end

    it "assigns requested list to @list" do
      list = FactoryGirl.create(:list)
      get :show, :id => list
      assigns(:list).should eq(list)
    end
  end

  describe "GET /new" do
    it "saves new list and redirects to list show" do
      expect {
        get :new
      }.to change(List, :count).by(1)
      response.should redirect_to list_path(assigns :list)
    end
  end
end
