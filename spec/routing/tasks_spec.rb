require 'spec_helper'

describe "routing to tasks" do
  it "shouldn't be routable" do
    expect(:delete => "/tasks/1").not_to be_routable
  end
end
