# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
  #
  # Toggle list edit form on list title click
  #
  $(document).on "click", ".listTitle a", (e) ->
    e.preventDefault()
    $(this).parent('.listTitle').hide()
    $(".listFormWrapper").show()
    $("#list_title").focus()

  $(document).on "click", ".listFormWrapper .cancel", (e) ->
    e.preventDefault()
    $(".listFormWrapper").hide()
    $(".listTitle").show()
