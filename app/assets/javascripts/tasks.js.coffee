# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  # set max posible height for quadrants
  sync_height('.quadrantWrapper')

  # sync on window resize
  $(window).resize sync_height

  # focus on visible form
  focus_on_task_form()

  # Move form to selected quadrant
  new TaskForm()

  # Enable Task sorting
  new SortableTasks()

  # Enable ajax task complete
  new CompletableTasks()

  # Show task editor form on double clicl
  new EditableTasks()


#
# Add sortable functionality for tasks
# this will enaable moving tasks between quadrants
#
class @SortableTasks
  constructor: () ->
    $(".quadrantWrapper").sortable
      items: "li.task"
      connectWith: ".quadrantWrapper"

    # Process quadrant order update
    .bind "sortupdate", (e, ui) =>
      $quadrant = $(e.currentTarget)

      quadrant_id = $quadrant.attr("id")
      list_uuid = $(".quadrantsWrapper").attr("data-uuid")

      tasks = $quadrant.find(".task").map (index, task) ->
        task_id = $(task).attr("data-task-id")
        task_id if task_id

      @update_quadrant_order $.makeArray(tasks), quadrant_id, list_uuid

  # Ajax update quadrant order
  update_quadrant_order: (task_ids, quadrant_id, list_uuid) ->
    data =
      task_ids: task_ids
      quadrant: quadrant_id

    $.ajax
      method: "POST"
      url: "/lists/#{list_uuid}/tasks/update_order"
      data: data
      dataType: "script"

#
# Add functionality for Add New Task
# it renders form in selected quadrant
#
class TaskForm
  constructor: () ->
    @quadrants = [1, 2, 3, 4]

    # Switch new task form
    $("[data-add-task]").on "click", (e) ->
      e.preventDefault()
      target_container_id = $(this).attr('data-add-task')

      move_task_form_to target_container_id

    # Cycle form on tab keypress
    $(document).on 'keydown', (e) =>
      key_code = e.keyCode || e.which

      if key_code == 9
        @move_to_next_quadrant()

    # Cancel task form edit
    $(document).on "click", ".edit_task .cancel", (e) ->
      # get parent form
      $task_form = $(e.currentTarget).closest(".taskForm")
      $task_form.prev(".task").show()
      $task_form.remove()

    # Cancel task create form
    $(document).on "click", ".new_task .cancel", (e) ->
      $task_form = $(e.currentTarget).closest(".taskForm")

      console.log $task_form.parent().find(".addTask")
      $task_form.parent().find(".addTask").removeClass "hidden"
      $task_form.hide()

    @move_to_next_quadrant = () =>
      unless form_is_in_focus()
        # if currently placed form is not in focus
        # tab will focus it in current position
        focus_on_task_form()
      else
        # Move form to next position
        move_task_form_to @get_next_quadrant()

    # get id for currently active quadrant
    @current_quadrant = () ->
      $('.quadrantWrapper.active').attr 'id'

    # check if task form is in focus
    form_is_in_focus = () ->
      $("#task_content").is ":focus"

    # get next quadrant
    @get_next_quadrant = () =>
      @quadrants[@current_quadrant() % @quadrants.length]


#
# Completable tasks with data-toggle="complete" attributes
#
class CompletableTasks
  constructor: () ->
    $(document).on 'click', '[data-toggle="complete"]', (e) ->
      request_url = $(this).data 'url'
      task_container = $(this).closest('.task')

      $.ajax
        url: request_url
        dataType: 'script'
        cache: true
        error: () ->
          alert("Server error. Cannot update task!")

    $(document).on "change", "#show_completed", (e) ->
      $(".task.completed").toggle()


#
# Editable tasks
#
class EditableTasks
  constructor: () ->
    console.log "editable tasks constructor"
    $(document).on "dblclick", ".task", (e) =>
      list_uuid = $(".quadrantsWrapper").attr("data-uuid")
      task_id = $(e.currentTarget).attr("data-task-id")
      $.getScript "/lists/#{list_uuid}/tasks/#{task_id}/edit"


#
# Set max posible height for task quadrants
# without scroll
#
@sync_height = (quadrant_selector) ->
  available_height = $(window).height() - $('.top-bar').height()
  $(quadrant_selector).height available_height/2
  $(quadrant_selector).css 'max-height', available_height/2


#
# Set focus on task form
#
@focus_on_task_form = () ->
  # focus task content input
  # timeout is because we are dinamically changing elements
  setTimeout () ->
    $("#task_content").focus()
    $(".quadrantWrapper.active").removeClass 'active'
    $("#task_content").closest('.quadrantWrapper').addClass 'active'
  , 10

#
# Show task form quadrant for provided quadrant_id
#
@move_task_form_to = (quadrant_id) ->
  target_container_selector = "##{quadrant_id}"
  target_container = $(target_container_selector)

  task_form = $('.taskForm').remove()
  task_form.find("#task_quadrant").val(quadrant_id)

  target_container.append task_form

  $(".addTask").removeClass "hidden"
  target_container.find(".addTask").addClass "hidden"

  # if form is previously hidden
  task_form.show()
  focus_on_task_form()
  # Scroll div to bottom when form is activated
  target_container.scrollTop(target_container.scrollTop() + target_container.height())

