$ ->
  $(document).ajaxSend (e, jqXHR, options) ->
    $("#global_loader").show()

  $(document).ajaxComplete (e, jqXHR, options) ->
    $("#global_loader").hide()
