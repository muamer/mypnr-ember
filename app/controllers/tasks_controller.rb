class TasksController < ApplicationController
  include ActiveModel::ForbiddenAttributesProtection

  # Attributes Protection
  def task_params
    params.require(:task).permit(:id, :content, :completed, :quadrant, :list_id)
  end

  # Attributes protection for update task orders
  def task_order_params
    params.permit(:task_ids, :quadrant_id)
  end

  #
  # POST /tasks
  #

  def create
    @list = List.find(params[:list_id])
    @task = @list.tasks.build task_params
    if @task.save
      respond_to do |format|
        format.html { redirect_to list_url(@list, quadrant: @task.quadrant) }
        format.js
      end 
    else
      respond_to do |format|
        format.html { redirect_to list_url(@list), :notice => "Cannot save task!" }
        format.js
      end 
    end
  end

  # 
  # GET /tasks/1
  #

  def edit
    @list = List.find(params[:list_id])
    @task = Task.find(params[:id])

    respond_to do |format|
      format.js
    end
  end

  #
  # PUT /tasks/1
  #

  def update
    @task = Task.find(params[:id])

    if @task.update_attributes(task_params)
      respond_to do |format|
        format.js
      end
    else
      respond_to do |format|
        format.js
      end
    end
  end

  #
  # GET /tasks/1
  #

  def complete
    @task = Task.find(params[:id])

    if @task.update_attribute(:completed, @task.completed ? false : true)
      respond_to do |format|
        format.html { redirect_to list_url(@task.list) }
        format.js
      end 
    else
      respond_to do |format|
        format.html { redirect_to list_url(@task.list), :notice => "Cannot change complete status!" }
        format.js
      end 
    end
  end

  #
  # DELETE /tasks/1
  #

  def destroy
    @task = Task.find(params[:id])
    @task.destroy

    respond_to do |format|
      format.html { redirect_to list_url(@task.list) }
      format.js
    end
  end

  #
  # POST /tasks/1
  #

  def update_order
    quadrant = params[:quadrant]
    params[:task_ids].each_with_index do |task_id, index|
      task = Task.find(task_id)
      task.update_attributes(:quadrant => quadrant, :sort => index)
    end

    respond_to do |format|
      format.js
    end
  end
end
