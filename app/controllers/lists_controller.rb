class ListsController < ApplicationController
  include ActiveModel::ForbiddenAttributesProtection

  def list_params
    params.require(:list).permit(:id, :title)
  end

  def index
    @lists = List.all

    respond_to do |format|
      format.html
    end
  end

  def show
    @list = List.find(params[:id])
    @task = Task.new
    @task.quadrant = params[:quadrant] || 2

    respond_to do |format|
      format.html
    end
  end

  def new
    @list = List.new
    @list.uuid = SecureRandom.uuid
    @list.save

    respond_to do |format|
      format.html { redirect_to list_url(@list) }
    end
  end

  def update
    @list = List.find(params[:id])
    @list.update_attributes(list_params)
  end
end
