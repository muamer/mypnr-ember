<% if @task.errors.any? %>
  alert("Cannot create task!")
<% else %>
  $(".taskForm").before "<%= j render @task %>"
  $("#task_content").val ""
  new SortableTasks()
<% end %>
