<% if @task.errors.any? %>
  alert("Cannot complete task!")
<% else %>
  $("#task-<%= @task.id %>").replaceWith "<%= j render @task %>"
  new SortableTasks()
<% end %>
