<% if @task.errors.any? %>
  alert("Cannot delete task!")
<% else %>
  $("#task-<%= @task.id %>").remove()
<% end %>
