<% if @task.errors.any? %>
  alert("Cannot update task!")
<% else %>
  task_id = "<%= @task.id %>"

  $("#edit_task_#{task_id}").parent().replaceWith """
    <%= j render @task %>
  """
  new SortableTasks()
<% end %>
