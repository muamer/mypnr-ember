<% if @list.errors.any? %>
  alert("Cannot update list name")
  $("#list_update_loader").hide()
<% else %>
  $(".listTitle a").html "<%= j @list.title %>"
  $(".listFormWrapper").hide()
  $(".listTitle").show()
  $("#list_update_loader").hide()
<% end %>
