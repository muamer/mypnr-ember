class Task < ActiveRecord::Base
  validates :content, :presence => true
  validates :list_id, :presence => true
  validates_inclusion_of :completed, :in => [true, false]
  validates :quadrant, :presence => true

  default_scope :order => "sort ASC"

  belongs_to :list

end
