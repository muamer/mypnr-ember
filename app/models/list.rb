class List < ActiveRecord::Base
  include ActiveUUID::UUID
  self.primary_key = :id

  validates :uuid, :presence => true
  has_many :tasks

  # Get list title, return uuid if title is empty
  def title
    self[:title].blank? ? uuid : self[:title]
  end

  # Get tasks for provided quadrant
  def tasks_for_quadrant(id)
    tasks.where(quadrant: id)
  end
end
