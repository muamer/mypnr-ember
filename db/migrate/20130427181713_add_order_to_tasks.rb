class AddOrderToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :sort, :integer, :nil => false, :default => 0
  end
end
