class AddQuadrantToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :quadrant, :integer, :nil => false
  end
end
