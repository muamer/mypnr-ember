class UseUuidAsForeignKeyForTasks < ActiveRecord::Migration
  def up
    change_column :tasks, :list_id, :uuid, :nil => false
  end

  def down
    change_column :tasks, :list_id, :integer, :nil => false
  end
end
