class AddUuidToLists < ActiveRecord::Migration
  def change
    add_column :lists, :uuid, :string, :nil => false
  end
end
