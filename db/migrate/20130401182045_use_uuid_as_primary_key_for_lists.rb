class UseUuidAsPrimaryKeyForLists < ActiveRecord::Migration
  def up
    change_column :lists, :id, :uuid, :primary_key => true
  end

  def down
    change_column :lists, :id, :integer, :primary_key => true
  end
end
